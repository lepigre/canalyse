CAnalyse: a minimal dependency analysis tool for C
==================================================

To install with [opam](http://opam.ocaml.org) simply pin the repository with:
```
opam pin add canalyse git+https://gitlab.mpi-sws.org/lepigre/canalyse.git
```
