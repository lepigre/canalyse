all:
	@dune build
.PHONY: all

clean:
	@dune clean
.PHONY: clean

install: all
	@dune install
.PHONY: install

uninstall:
	@dune uninstall
.PHONY: uninstall
