(** [realpath path] returns the absolute canonical path to file [path]. If the
    path [path] is invalid (i.e., it does not describe an existing file), then
    the exception [Invalid_argument] is raised. *)
external realpath : string -> string = "c_realpath"

(** [relative_path root file] computes a relative filepath for [file] with its
    origin at [root]. Note that [Invalid_argument] is raised on errors. *)
let relative_path : string -> string -> string = fun root file ->
  let root = realpath root in
  let file = realpath file in
  let root_len = String.length root in
  let full_len = String.length file in
  if root_len > full_len then
    invalid_arg "Extra.Filename.relative_path";
  let file_root = String.sub file 0 root_len in
  if file_root <> root then
    invalid_arg "Extra.Filename.relative_path";
  String.sub file (root_len + 1) (full_len - root_len - 1)

module SSet = Set.Make(String)
module SMap = Map.Make(String)


