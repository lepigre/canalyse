open Util
open Common

let write : out_channel -> Deps.t -> filepath -> unit = fun oc deps root ->
  let file_id =
    let node_map = Hashtbl.create 73 in
    let counter = ref (-1) in
    let fn file _ = incr counter; Hashtbl.add node_map file !counter in
    Deps.breadth_first deps fn root;
    fun f -> try Hashtbl.find node_map f with Not_found -> assert false
  in
  let print_node is_root file =
    let fmt : (_, _, _) format =
      match is_root with
      | true  -> "  node%i [label = %S, shape=box, color=red];\n"
      | false -> "  node%i [label = %S];\n"
    in
    Printf.fprintf oc fmt (file_id file) file
  in
  let print_node =
    let printed_nodes = ref SSet.empty in
    let print_node is_root file =
      if not (SSet.mem file !printed_nodes) then
        begin
          print_node is_root file;
          printed_nodes := SSet.add file !printed_nodes
        end
    in
    print_node
  in
  let print_edge src dst =
    Printf.printf "  node%i -> node%i;\n" (file_id src) (file_id dst)
  in
  Printf.fprintf oc "// Dependencies for [%s].\n" root;
  Printf.fprintf oc "digraph deps {\n";
  print_node true root;
  let ext_deps = ref SSet.empty in
  let fn file deps =
    let open Deps in
    List.iter (print_node false) deps.deps_internal;
    List.iter (fun f -> ext_deps := SSet.add f !ext_deps) deps.deps_external;
    List.iter (print_edge file) deps.deps_internal
  in
  Deps.breadth_first deps fn root;
  Printf.fprintf oc "}\n%!"
