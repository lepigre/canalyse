open Common
open Util

type deps = {
  deps_internal : filepath list; (** Filepath to internal deps. *)
  deps_external : filepath list; (** Include path for external deps. *)
}

type t = (filepath, deps) Hashtbl.t

let create : unit -> t = fun _ -> Hashtbl.create 73
let mem : t -> filepath -> bool = Hashtbl.mem
let add : t -> filepath -> deps -> unit = Hashtbl.add
let find : t -> filepath -> deps = Hashtbl.find

type action = filepath -> deps -> unit

let breadth_first : t -> action -> filepath -> unit = fun all_deps f c_file ->
  let rec loop files_done files_todo =
    match files_todo with
    | []                   -> ()
    | c_file :: files_todo ->
        let files_done = SSet.add c_file files_done in
        let deps =
          try find all_deps c_file with Not_found ->
            Printf.eprintf "Cannot find deps for [%s]\n%!" c_file; exit 1
        in
        f c_file deps;
        let files_todo =
          let todo f =
            not (SSet.mem f files_done) && not (List.mem f files_todo)
          in
          files_todo @ List.filter todo deps.deps_internal
        in
        loop files_done files_todo

  in
  loop SSet.empty [c_file]

let depth_first : t -> action -> filepath -> unit = fun all_deps f c_file ->
  let rec loop files_done files_todo =
    match files_todo with
    | []                   -> ()
    | c_file :: files_todo ->
        let files_done = SSet.add c_file files_done in
        let deps =
          try find all_deps c_file with Not_found ->
            Printf.eprintf "Cannot find deps for [%s]\n%!" c_file; exit 1
        in
        f c_file deps;
        let files_todo =
          let todo f = not (SSet.mem f files_done) in
          let new_todo = List.filter todo deps.deps_internal in
          new_todo @ List.filter (fun f -> not (List.mem f new_todo)) files_todo
        in
        loop files_done files_todo
  in
  loop SSet.empty [c_file]
