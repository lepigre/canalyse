open Common
open Util

let normalize_path : filepath -> filepath = fun path ->
  relative_path workdir (realpath path)

type order = Breadth_first | Depth_first

type output = Text | Graphviz

type config = {
  cpp_I : directory list;      (** Directories to search for Header files. *)
  cpp_include : filepath list; (** Extra included files. *)
  traversal : order;           (** Traversal order for the dependencies. *)
  output : output;             (** Output format. *)
}

(** [find_header dirs header] locate the file corresponding to the header file
    [header] considering [dirs] as the set of include directories to search in
    (i.e., directories given with the -I option). The exception [Not_found] is
    raised if no appropriate file exists. *)
let find_header : directory list -> filepath -> filepath = fun dirs header ->
  let rec find dirs =
    match dirs with
    | []        -> raise Not_found
    | d :: dirs ->
        let file = Filename.concat d header in
        if Sys.file_exists file then file else find dirs
  in
  normalize_path (find dirs)

let file_deps : directory list -> filepath -> Deps.deps = fun dirs c_file ->
  let rec sort deps_internal deps_external deps =
    match deps with
    | []        -> Deps.{deps_internal; deps_external}
    | d :: deps ->
        try sort (find_header dirs d :: deps_internal) deps_external deps
        with Not_found -> sort deps_internal (d :: deps_external) deps
  in
  sort [] [] (List.rev (Cdeps.read_deps c_file))

let report : string -> Deps.deps -> unit = fun c_file deps ->
  Printf.printf "File [%s] " c_file;
  match (deps.deps_internal, deps.deps_external) with
  | ([]      , []      ) -> Printf.printf "has no dependency.\n"
  | (int_deps, ext_deps) ->
      Printf.printf "depends on:\n";
      List.iter (Printf.printf "- [%s]\n") int_deps;
      List.iter (Printf.printf "- [%s] (external)\n") ext_deps

(** [run cfg c_file] runs the analysis on file [c_file] with config [cfg]. *)
let run : config -> string -> unit = fun cfg c_file ->
  (* All computed dependencies (i.e., everything at the end). *)
  let all_deps = Deps.create () in
  (* Deps for the root file (special handling of --include). *)
  let deps =
    let deps = file_deps cfg.cpp_I (normalize_path c_file) in
    {deps with deps_internal = cfg.cpp_include @ deps.deps_internal}
  in
  Deps.add all_deps c_file deps;
  (* Compute all transitive deps and detect cycles. *)
  let rec loop todo =
    match todo with
    | []                                                 -> ()
    | (stack, c_file) :: todo when List.mem c_file stack ->
        (* Cycle detected, report. *)
        Printf.eprintf "Deps cycle detected on [%s]. Stack:\n" c_file;
        let print_elt s =
          Printf.eprintf "- [%s]\n" s;
          if s = c_file then raise Exit
        in
        let _ =
          try List.iter print_elt stack with Exit ->
            Printf.eprintf "%!"
        in
        assert (Deps.mem all_deps c_file); (* Sanity check. *)
        loop todo (* We just continue, cycles are a feature. *)
    | (stack, c_file) :: todo                            ->
        (* We don't stop so that we can detect all cycles. *)
        if Deps.mem all_deps c_file then loop todo else
        let deps =
          let deps = file_deps cfg.cpp_I c_file in
          Deps.add all_deps c_file deps; deps
        in
        let new_todo =
          List.map (fun f -> (c_file :: stack, f)) deps.deps_internal
        in
        loop (new_todo @ todo)
  in
  loop (List.map (fun f -> ([c_file], f)) deps.deps_internal);
  (* Do some printing. *)
  match cfg.output with
  | Text     ->
      let traverse =
        match cfg.traversal with
        | Breadth_first -> Deps.breadth_first 
        | Depth_first   -> Deps.depth_first 
      in
      traverse all_deps report c_file
  | Graphviz ->
      Graphviz.write stdout all_deps c_file;
      Printf.printf "%!"
