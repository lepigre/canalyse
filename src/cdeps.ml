open Common

let read_deps : filepath -> filepath list = fun path ->
  let cmd =
    "grep '^#include *<' '" ^ path ^ "' | sed -e 's/.*<\\(.*\\)>.*/\\1/'"
  in
  let ic = Unix.open_process_in cmd in
  let rec loop () =
    match input_line ic with
    | exception End_of_file -> []
    | l                     -> l :: loop ()
  in
  let deps = loop () in
  match Unix.close_process_in ic with
  | Unix.WEXITED(0) -> deps
  | _               -> assert false
