open Cmdliner
open Canalyse

let cpp_I =
  let doc =
    "Add the directory $(docv) to the list of directories to be searched for \
     header files."
  in
  let i = Arg.(info ["I"] ~docv:"DIR" ~doc) in
  Arg.(value & opt_all dir [] & i)

let cpp_include =
  let doc =
    "Add a dependency to the file $(docv) as if there was an include on it \
     at the beginning of the source file."
  in
  let i = Arg.(info ["include"] ~docv:"FILE" ~doc) in
  Arg.(value & opt_all file [] & i)

let depth_first =
  let doc =
    "Traverse the dependency graph using depth-first search instead of the \
     default breadth-first search."
  in
  Arg.(value & flag & info ["depth-first"] ~doc)

let graphviz =
  let doc =
    "Output dependency graph using the Graphviz format."
  in
  Arg.(value & flag & info ["graphviz"] ~doc)


let config =
  let build cpp_I cpp_include depth_first graphviz =
    let traversal = if depth_first then Depth_first else Breadth_first in
    let output = if graphviz then Graphviz else Text in
    {cpp_I; cpp_include; traversal; output}
  in
  Term.(pure build $ cpp_I $ cpp_include $ depth_first $ graphviz)

let c_file =
  let doc = "C language source file." in
  Arg.(required & pos 0 (some non_dir_file) None & info [] ~docv:"FILE" ~doc)

let _ =
  let open Term in
  let term = pure run $ config $ c_file in
  let doc = "Naive dependency analyser for C." in
  exit @@ eval (term, info ~doc "canalyse")
