open Common

type deps = {
  deps_internal : filepath list; (** Filepath to internal deps. *)
  deps_external : filepath list; (** Include path for external deps. *)
}

type t

val create : unit -> t

val mem : t -> filepath -> bool

val add : t -> filepath -> deps -> unit

val find : t -> filepath -> deps

val breadth_first : t -> (filepath -> deps -> unit) -> filepath -> unit

val depth_first : t -> (filepath -> deps -> unit) -> filepath -> unit
